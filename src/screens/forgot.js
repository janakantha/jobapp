import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const forgot = ({navigation}) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ height: hp('100%'), justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 35, lineHeight: 36, color: '#EEAB02' }}>job</Text>
                    <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 35, lineHeight: 36, color: '#000' }}>Agent</Text>
                </View>
                <View style={{ marginTop: 20, width: wp('90%'), justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular',textAlign:'center', fontSize: 14, color: '#636363' }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </Text>
                </View>
                <View style={{ marginTop: 30, width: wp('90%'), justifyContent: 'center', alignItems: 'center' }}>

                    <View style={{ flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                        <Image style={{ height: 22, width: 22, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/at.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>E mail</Text>
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('Verify')} style={{
                        marginTop: 45, width: wp('80%'), height: 43, backgroundColor: '#FFC803', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, lineHeight: 17, color: '#fff' }}>Get OTP Code</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default forgot;
