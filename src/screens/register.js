import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const register = ({ navigation }) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ height: hp('100%'), justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 35, color: '#EEAB02' }}>job</Text>
                    <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 35, color: '#000' }}>Agent</Text>
                </View>
                <View style={{ marginTop: 40, width: wp('90%'), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                        <Image style={{ height: 22, width: 22, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/at.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>E mail</Text>
                    </View>
                    <View style={{ marginTop: 15, flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                        <Image style={{ height: 22, width: 22, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/pass.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Password</Text>
                    </View>
                    <View style={{ marginTop: 15, flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                        <Image style={{ height: 22, width: 22, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/repass.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Re-Enter Password</Text>
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')} style={{
                        marginTop: 35, width: wp('80%'), height: 43, backgroundColor: '#FFC803', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, lineHeight: 17, color: '#fff' }}>Sign-Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default register;
