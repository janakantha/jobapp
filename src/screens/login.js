import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const login = ({navigation}) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ height: hp('100%'), justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 35, color: '#EEAB02' }}>job</Text>
                    <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 35, color: '#000' }}>Agent</Text>
                </View>
                <View style={{ marginTop: 40, width: wp('90%'), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                        <Image style={{ height: 22, width: 22, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/at.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>E mail</Text>
                    </View>
                    <View style={{ marginTop: 15, flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                        <Image style={{ height: 22, width: 22, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/pass.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Password</Text>
                    </View>
                    <View style={{ marginTop: 5, flexDirection: 'row', width: wp('80%'), justifyContent: 'flex-end', alignItems: 'flex-start', height: 45 }}>
                        <TouchableOpacity onPress={()=>navigation.navigate('Forgot')}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, lineHeight: 15, color: '#636363' }}>Forgot Password ?</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={()=>navigation.navigate('Home')} style={{
                        width: wp('80%'), height: 43, backgroundColor: '#FFC803', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, lineHeight: 17, color: '#fff' }}>Sign-In</Text>
                    </TouchableOpacity>
                    <View style={{ marginTop: 25, flexDirection: 'row', width: wp('80%'), justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>------------  </Text>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>OR</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>  ------------</Text>
                    </View>
                    <View style={{ marginTop: 5, flexDirection: 'row', width: wp('80%'), justifyContent: 'center', alignItems: 'center', }}>
                        <Image style={{ height: 44, width: 44, resizeMode: 'contain', marginRight: 15 }} source={require('../assets/gg.png')} />
                        <Image style={{ height: 44, width: 44, resizeMode: 'contain' }} source={require('../assets/fb.png')} />
                    </View>
                    < TouchableOpacity onPress={()=>navigation.navigate('Register')} style={{ marginTop: 55, flexDirection: 'row', width: wp('80%'), height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, borderColor: '#636363' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, color: '#636363' }}>Need Account ? Sign-Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default login;
