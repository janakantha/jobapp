import React, { useRef } from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import RBSheet from "react-native-raw-bottom-sheet";

const jobs = ({ navigation }) => {
    const refRBSheet = useRef();
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ paddingHorizontal: wp('5%') }}>
                <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View />
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 20, color: '#EEAB02' }}>job</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 20, color: '#000' }}>Agent</Text>
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={{ alignItems: 'center' }}>
                        <Image style={{ height: 30, width: 30 }} source={require('../assets/user.png')} />
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: wp('5%') }}>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>JOB SEARCH</Text>
                    <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', width: wp('76%'), justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#EAEAEA', borderRadius: 8 }}>
                            <Image style={{ height: 18, width: 18, marginRight: 10 }} source={require('../assets/find.png')} />
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Search</Text>
                        </View>
                        <TouchableOpacity onPress={() => refRBSheet.current.open()} style={{ flexDirection: 'row', width: 45, height: 45, backgroundColor: '#FFC803', borderRadius: 8, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ height: 18, width: 18 }} source={require('../assets/filt.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 14, color: '#636363' }}>Sort by:</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Date posted</Text>
                            <Image style={{ height: 12, width: 12, resizeMode: 'contain', marginLeft: 5, tintColor: '#636363' }} source={require('../assets/down.png')} />
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('Detail')}
                        style={{
                            marginVertical: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10, height: 135, backgroundColor: '#fff', borderRadius: 8,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                        }}>
                        <View style={{ width: wp('10%'), paddingTop: 5, height: 125, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 43, width: 43, }} source={require('../assets/i1.png')} />
                        </View>
                        <View style={{ width: wp('80%') - 20, height: 125, padding: 10 }}>
                            <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>SENIOR SOFTWARE DEVELOPER</Text>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#636363' }}>(RE-ADVERTISED)</Text>
                            <View style={{ marginTop: 5, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Company:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06' }}>I{'&'}M Bank</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363' }}>Location:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#E06D06' }}>Nairobi, Kenya</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, marginTop: 20, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 15, marginRight: 10 }} source={require('../assets/clock.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 15 }}>Posted 3 days ago</Text>
                                <View style={{ height: 6, width: 6, borderRadius: 10, backgroundColor: '#C4C4C4', marginRight: 15 }} />
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 10 }}>420 Views</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('Detail')}
                        style={{
                            marginVertical: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10, height: 135, backgroundColor: '#fff', borderRadius: 8,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,

                            elevation: 5,
                        }}>
                        <View style={{ width: wp('10%'), paddingTop: 5, height: 125, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 43, width: 43, }} source={require('../assets/i2.png')} />
                        </View>
                        <View style={{ width: wp('80%') - 20, height: 125, padding: 10 }}>
                            <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>MEDIA MANAGER</Text>
                            <View style={{ marginTop: 10, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Company:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06' }}>Lombrisol</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363' }}>Location:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#E06D06' }}>Marocco, Ait Meloul</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, marginTop: 20, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 15, marginRight: 10 }} source={require('../assets/clock.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 15 }}>Posted 6 days ago</Text>
                                <View style={{ height: 6, width: 6, borderRadius: 10, backgroundColor: '#C4C4C4', marginRight: 15 }} />
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 10 }}>428 Views</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('Detail')}
                        style={{
                            marginVertical: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10, height: 135, backgroundColor: '#fff', borderRadius: 8,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,

                            elevation: 5,
                        }}>
                        <View style={{ width: wp('10%'), paddingTop: 5, height: 125, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 43, width: 43, }} source={require('../assets/i3.png')} />
                        </View>
                        <View style={{ width: wp('80%') - 20, height: 125, padding: 10 }}>
                            <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>WEB DEVELOPER</Text>
                            <View style={{ marginTop: 10, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Company:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06' }}>Cool Hubs</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363' }}>Location:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#E06D06' }}>Nigeria, Owerri</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, marginTop: 20, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 15, marginRight: 10 }} source={require('../assets/clock.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 15 }}>Posted 2 days ago</Text>
                                <View style={{ height: 6, width: 6, borderRadius: 10, backgroundColor: '#C4C4C4', marginRight: 15 }} />
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 10 }}>160 Views</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            <View style={{
                position: 'absolute', left: 0, bottom: 0, width: wp('100%'), height: 55, backgroundColor: '#fff', borderTopLeftRadius: 15, borderTopRightRadius: 15,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.27,
                shadowRadius: 4.65,
                elevation: 6,
                flexDirection: 'row', justifyContent: 'space-around', alignItems: 'flex-end', paddingBottom: 4
            }}>
                <TouchableOpacity onPress={() => navigation.navigate('Jobs')} style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 12, tintColor: '#FFC803' }} source={require('../assets/doc.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#FFC803', marginTop: 4 }}>JOBS</Text>
                </TouchableOpacity>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 18 }} source={require('../assets/chat.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>MESSAGES</Text>
                </View>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 18, tintColor: '#636363' }} source={require('../assets/news.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>MY FUZU</Text>
                </View>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 16, width: 18 }} source={require('../assets/lap.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>COURSES</Text>
                </View>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 18 }} source={require('../assets/more.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>MORE</Text>
                </View>
            </View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                dragFromTopOnly
                height={hp('70%')}
                customStyles={{
                    wrapper: {
                        backgroundColor: 'rgba(54,54,54,0.8)',
                    },
                    container: {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    },
                    draggableIcon: {
                        backgroundColor: "#EAEAEA"
                    }
                }}
            >
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ paddingHorizontal: wp('5%') }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>FILTER</Text>
                        <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 14, color: '#636363' }}>INDUSTRY</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#BABABA' }}>CLEAR FILTERS</Text>
                                <Image style={{ height: 15, width: 15, resizeMode: 'contain', marginLeft: 5, tintColor: '#BABABA' }} source={require('../assets/pen.png')} />
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: 10, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <View style={{ height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Accounting {'&'} Legal</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#FFC803', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#262626' }}>Business Services</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: 15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <View style={{ height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Construction</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Finance</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Health Care</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: 15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <View style={{ height: 33, paddingHorizontal: 15, backgroundColor: '#FFC803', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#262626' }}>IT</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Manufactiruing</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#FFC803', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#262626' }}>Media</Text>
                            </View>
                        </View>
                        <View style={{ width: wp('90%'), marginTop: 15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <View style={{ height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Food</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Retail</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Hospitality {'&'} Events</Text>
                            </View>
                        </View>
                        <Text style={{ marginTop: 13, fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>LOCATION</Text>
                        <View style={{ marginTop: 10, flexDirection: 'row', width: wp('90%'), justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10, height: 45, backgroundColor: '#F3F3F3', borderRadius: 8 }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Location</Text>
                            <Image style={{ height: 18, width: 18, resizeMode: 'contain' }} source={require('../assets/pin.png')} />

                        </View>
                        <Text style={{ marginTop: 13, fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>SALARY  ESTIMATE</Text>
                        <View style={{ width: wp('90%'), marginTop: 15, flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: wp('27%'), height: 1, backgroundColor: '#BABABA' }} />
                            <View style={{ width: wp('4.5%'), height: wp('4.5%'), backgroundColor: '#FFC803', borderRadius: 10 }} />
                            <View style={{ width: wp('27%'), height: 3, backgroundColor: '#FFC803' }} />
                            <View style={{ width: wp('4.5%'), height: wp('4.5%'), backgroundColor: '#FFC803', borderRadius: 10 }} />
                            <View style={{ width: wp('27%'), height: 1, backgroundColor: '#BABABA' }} />
                        </View>
                        <View style={{ width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>$ 1,500</Text>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>$ 3,500</Text>
                        </View>
                        <Text style={{ marginTop: 13, fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>EXPERIENCE LEVEL</Text>
                        <View style={{ width: wp('90%'), marginTop: 15, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Entry</Text>
                            </View>
                            <View style={{ height: 33, paddingHorizontal: 15, backgroundColor: '#FFC803', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#262626' }}>Middle</Text>
                            </View>
                            <View style={{ marginLeft: 15, height: 33, paddingHorizontal: 15, backgroundColor: '#F3F3F3', justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 14, color: '#636363' }}>Senior</Text>
                            </View>
                        </View>
                        <Text style={{ marginTop: 13, fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>JOB TYPE</Text>
                    </View>
                    <View style={{ height: hp('4%') }} />
                </ScrollView>
            </RBSheet>
        </View>
    );
};

export default jobs;
