import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, ViewPropTypes } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const detail = ({ navigation }) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ paddingHorizontal: wp('5%') }}>
                <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ height: 13, width: 13, resizeMode: 'contain' }} source={require('../assets/back.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>JOB SEARCH</Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 20, color: '#EEAB02' }}>job</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 20, color: '#000' }}>Agent</Text>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ height: 30, width: 30 }} source={require('../assets/user.png')} />
                    </View>
                </View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: wp('5%') }}>
                    <View
                        style={{
                            marginVertical: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10, height: 100
                        }}>
                        <View style={{ width: wp('10%'), paddingTop: 5, height: 90, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 43, width: 43, }} source={require('../assets/i1.png')} />
                        </View>
                        <View style={{ width: wp('80%') - 20, height: 90, padding: 10 }}>
                            <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>SENIOR SOFTWARE DEVELOPER</Text>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#636363' }}>(RE-ADVERTISED)</Text>
                            <View style={{ marginTop: 5, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/build.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06', marginRight: 10, textDecorationLine: 'underline' }}>I{'&'}M Bank Limited</Text>
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/pin.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Nairobi, Kenya</Text>
                            </View>
                            <View style={{ marginTop: 5, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/clock.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363', marginRight: 10, }}>Posted 3 days ago</Text>
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>420 Views</Text>
                            </View>
                        </View>
                    </View>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>Job Summary</Text>
                    <Text style={{ marginTop: 5, fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#636363' }}>PURPOSE</Text>
                    <Text style={{ marginTop: 5, fontFamily: 'SF-Pro-Display-Regular', fontSize: 16, lineHeight: 19, color: '#949494' }}>Engineers at iCube build products and services that impact the day to day lives of our customers and internal teams. Your contributions will ensure that tangible value is continuously delivered to the end users. A successful candidate will be expected to work closely with the product & design team to ensure timely delivery of great products. He/she will also need to be willing to learn and passionate about making contributions towards the team’s success.</Text>
                    <Text style={{ marginTop: 5, fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#636363' }}>THIS MAY BE YOUR DREAM JOB IF YOU:</Text>
                    <Text style={{ marginTop: 5, fontFamily: 'SF-Pro-Display-Regular', fontSize: 16, lineHeight: 19, color: '#949494' }}>Have a strong core understanding of software engineering best practices</Text>
                    <Text style={{ marginTop: 10, fontFamily: 'SF-Pro-Display-Regular', fontSize: 16, lineHeight: 19, color: '#949494' }}>• Possess fantastic problem solving, debugging and troubleshooting skills</Text>
                    <Text style={{ marginTop: 10, fontFamily: 'SF-Pro-Display-Regular', fontSize: 16, lineHeight: 19, color: '#949494' }}>• Have an ability to prioritize and handle multiple tasks competently</Text>
                    <View style={{ marginTop: 10, width: wp('90%'), height: 1, backgroundColor: '#EEEEEE' }} />
                    <Text style={{ marginTop: 5, fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>Similar vacancies</Text>
                </View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}
                    style={{ paddingLeft: wp('1.5%') }}>
                    <View style={{
                        marginVertical: 10, marginHorizontal: 13, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10, height: 110, backgroundColor: '#fff', borderRadius: 8,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    }}>
                        <View style={{ width: wp('10%'), paddingTop: 5, height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 43, width: 43, }} source={require('../assets/i2.png')} />
                        </View>
                        <View style={{ width: wp('80%') - 20, height: 100, padding: 10 }}>
                            <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>MEDIA MANAGER</Text>
                            <View style={{ marginTop: 10, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Company:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06' }}>Lombrisol</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363' }}>Location:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#E06D06' }}>Marocco, Ait Meloul</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, marginTop: 10, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 15, marginRight: 10 }} source={require('../assets/clock.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 15 }}>Posted 6 days ago</Text>
                                <View style={{ height: 6, width: 6, borderRadius: 10, backgroundColor: '#C4C4C4', marginRight: 15 }} />
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 10 }}>428 Views</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{
                        marginVertical: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10, height: 110, backgroundColor: '#fff', borderRadius: 8,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    }}>
                        <View style={{ width: wp('10%'), paddingTop: 5, height: 100, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 43, width: 43, }} source={require('../assets/i3.png')} />
                        </View>
                        <View style={{ width: wp('80%') - 20, height: 100, padding: 10 }}>
                            <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>MEDIA MANAGER</Text>
                            <View style={{ marginTop: 10, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Company:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06' }}>Lombrisol</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363' }}>Location:</Text>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#E06D06' }}>Marocco, Ait Meloul</Text>
                            </View>
                            <View style={{ width: wp('80%') - 30, marginTop: 10, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Image style={{ height: 15, width: 15, marginRight: 10 }} source={require('../assets/clock.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 15 }}>Posted 6 days ago</Text>
                                <View style={{ height: 6, width: 6, borderRadius: 10, backgroundColor: '#C4C4C4', marginRight: 15 }} />
                                <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                                <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 13, color: '#636363', marginRight: 10 }}>428 Views</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={{ paddingHorizontal: wp('5%') }}>
                    <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{
                            width: 43, height: 43, backgroundColor: '#F3F3F3', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                        }}>
                            <Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={require('../assets/book.png')} />
                        </View>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Apply')}
                            style={{
                                width: wp('62%'), height: 43, backgroundColor: '#FFC803', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                            }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, lineHeight: 17, color: '#fff' }}>Apply Now</Text>
                        </TouchableOpacity>
                        <View style={{
                            width: 43, height: 43, backgroundColor: '#F3F3F3', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                        }}>
                            <Image style={{ height: 20, width: 20, resizeMode: 'contain' }} source={require('../assets/share.png')} />
                        </View>
                    </View>
                </View>
                <View style={{ height: 15 }} />
            </ScrollView>
        </View>
    );
};

export default detail;
