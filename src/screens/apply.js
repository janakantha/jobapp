import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, ViewPropTypes } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const apply = ({ navigation }) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ paddingHorizontal: wp('5%') }}>
                <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ height: 13, width: 13, resizeMode: 'contain' }} source={require('../assets/back.png')} />
                        <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>JOB DETAIL</Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 20, color: '#EEAB02' }}>job</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 20, color: '#000' }}>Agent</Text>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ height: 30, width: 30 }} source={require('../assets/user.png')} />
                    </View>
                </View>
                <View
                    style={{
                        marginVertical: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', padding: 10, backgroundColor: '#fff', borderRadius: 8,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                    }}>
                    <View style={{ width: wp('10%'), paddingTop: 5, height: 90, justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image style={{ height: 43, width: 43, }} source={require('../assets/i1.png')} />
                    </View>
                    <View style={{ width: wp('80%') - 20, padding: 10 }}>
                        <Text style={{ width: wp('70%'), fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, lineHeight: 13, color: '#636363' }}>SENIOR SOFTWARE DEVELOPER</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#636363' }}>(RE-ADVERTISED)</Text>
                        <View style={{ marginTop: 5, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/build.png')} />
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#E06D06', marginRight: 10, textDecorationLine: 'underline' }}>I{'&'}M Bank Limited</Text>
                            <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/pin.png')} />
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>Nairobi, Kenya</Text>
                        </View>
                        <View style={{ marginTop: 5, width: wp('80%') - 30, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/clock.png')} />
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363', marginRight: 10, }}>Posted 3 days ago</Text>
                            <Image style={{ height: 15, width: 20, marginRight: 10, resizeMode: 'contain' }} source={require('../assets/eye.png')} />
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, lineHeight: 15, color: '#636363' }}>420 Views</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Profile')}
                            style={{

                                marginTop: 15, width: wp('62%'), height: 43, backgroundColor: '#FFC803', borderRadius: 8, justifyContent: 'center', alignItems: 'center',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                            }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, lineHeight: 17, color: '#fff' }}>Apply Via JobAgent</Text>
                        </TouchableOpacity>
                        <View style={{ marginTop: 15, flexDirection: 'row', width: wp('62%'), height: 45, justifyContent: 'flex-start', alignItems: 'center', borderRadius: 8, backgroundColor: '#EAEAEA', paddingHorizontal: 10 }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>Full Name</Text>
                        </View>
                        <View style={{ marginTop: 15, flexDirection: 'row', width: wp('62%'), height: 45, justifyContent: 'flex-start', alignItems: 'center', borderRadius: 8, backgroundColor: '#EAEAEA', paddingHorizontal: 10 }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>Contact Number</Text>
                        </View>
                        <View style={{ marginTop: 15, flexDirection: 'row', width: wp('62%'), height: 45, justifyContent: 'flex-start', alignItems: 'center', borderRadius: 8, backgroundColor: '#EAEAEA', paddingHorizontal: 10 }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>E mail</Text>
                        </View>
                        <View style={{ marginTop: 15, flexDirection: 'row', width: wp('62%'), height: 45, justifyContent: 'space-between', alignItems: 'center', borderRadius: 8, backgroundColor: '#EAEAEA', paddingLeft: 10 }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>Upload CV</Text>
                            <TouchableOpacity style={{ width: 45, height: 45, backgroundColor: '#636363', borderTopRightRadius: 8, justifyContent: 'center', alignItems: 'center', borderBottomRightRadius: 8 }}>
                                <Image style={{ height: 20, width: 20, tintColor: '#fff', resizeMode: 'contain' }} source={require('../assets/upload.png')} />
                            </TouchableOpacity>
                        </View>
                        < TouchableOpacity onPress={() => navigation.navigate('Profile')} style={{ marginTop: 15, flexDirection: 'row', width: wp('62%'), height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, backgroundColor: '#636363' }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, color: '#fff' }}>Apply</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        </View>
    );
};

export default apply;
