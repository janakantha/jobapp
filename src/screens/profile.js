import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const proifile = ({ navigation }) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ paddingHorizontal: wp('5%') }}>
                <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View />
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 20, color: '#EEAB02' }}>job</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 20, color: '#000' }}>Agent</Text>
                    </View>
                    <View />
                </View>
            </View>
            <View style={{ width: wp('100%'), height: hp('36%'), justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ height: 90, width: 90 }} source={require('../assets/profile.png')} />
                <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 20, color: '#636363' }}>DEV Dev</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{ height: 16, width: 16 }} source={require('../assets/eye.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, color: '#636363', marginLeft: 10 }}>457 Views</Text>
                    <Image style={{ height: 16, width: 16, marginLeft: 15 }} source={require('../assets/star.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, color: '#636363', marginLeft: 10 }}>53 Stars</Text>
                </View>
            </View>
            <View style={{ width: wp('100%'), height: hp('36%'), justifyContent: 'flex-start', alignItems: 'center' }}>
                < TouchableOpacity style={{ marginTop: 25, flexDirection: 'row', width: wp('70%'), height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, borderColor: '#636363' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, color: '#636363' }}>Option 1</Text>
                </TouchableOpacity>
                < TouchableOpacity style={{ marginTop: 15, flexDirection: 'row', width: wp('70%'), height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, borderColor: '#636363' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, color: '#636363' }}>Option 2</Text>
                </TouchableOpacity>
                < TouchableOpacity style={{ marginTop: 15, flexDirection: 'row', width: wp('70%'), height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, borderColor: '#636363' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, color: '#636363' }}>Option 3</Text>
                </TouchableOpacity>
                < TouchableOpacity onPress={() => navigation.navigate('Login')} style={{ marginTop: 15, flexDirection: 'row', width: wp('70%'), height: 45, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, borderColor: '#FF0000' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 16, color: '#FF0000' }}>Log Out</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default proifile;
