import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Card1 from '../components/Card1';
import Card2 from '../components/Card2';
import Card3 from '../components/Card3';

const home = ({navigation}) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ paddingHorizontal: wp('5%') }}>
                <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View />
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Heavy', fontSize: 20, color: '#EEAB02' }}>job</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Text-LightItalic', fontSize: 20, color: '#000' }}>Agent</Text>
                    </View>
                    <TouchableOpacity onPress={()=>navigation.navigate('Profile')} style={{ alignItems: 'center' }}>
                        <Image style={{ height: 30, width: 30 }} source={require('../assets/user.png')} />
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: wp('5%') }}>
                    <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>UPDATES</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, color: '#BABABA' }}>COLLAPSE</Text>
                            <Image style={{ height: 14, width: 14, resizeMode: 'contain', marginLeft: 2 }} source={require('../assets/up.png')} />
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 10 }}>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={[1, 2, 3]}
                        renderItem={({ item }) => {
                            switch (item) {
                                case 1:
                                    return <Card1 />
                                case 2:
                                    return <Card2 />
                                case 3:
                                    return <Card3 />
                                default:
                                    break;
                            }
                        }}
                    />
                </View>
                <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: 10, width: 10, backgroundColor: '#FFC803', borderRadius: 10 }} />
                    <View style={{ marginLeft: 15, height: 10, width: 10, backgroundColor: '#BBB', borderRadius: 10 }} />
                    <View style={{ marginLeft: 15, height: 10, width: 10, backgroundColor: '#BBB', borderRadius: 10 }} />
                </View>
                <View style={{ paddingHorizontal: wp('5%') }}>
                    <View style={{ marginTop: 15, width: wp('90%'), height: 2, backgroundColor: '#EEE' }} />
                    <Text style={{ marginTop: 10, fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>LATEST NEWS</Text>
                    <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image style={{ height: 80, width: 80 }} source={require('../assets/person.png')} />
                        <View style={{ marginLeft: 10, justifyContent: 'space-between', alignItems: 'flex-start' }}>
                            <Text style={{ width: wp('60%'), fontFamily: 'SF-Pro-Text-Bold', fontSize: 12, color: '#636363' }}>LEAVING WORK AT WORK WHEN HOME IS THE NEW OFFICE</Text>
                            <Text style={{ fontFamily: 'SF-Pro-Text-Regular', fontSize: 12, color: '#E06D06' }}>May 5, 2020</Text>
                            <Text style={{ width: wp('60%'), fontFamily: 'SF-Pro-Text-Regular', fontSize: 12, color: '#636363' }}>PHOTO CREDIT: RETHA FERGUSON LEAVING WORK AT WORK IS NEVER[...]</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 15, width: wp('90%'), height: 2, backgroundColor: '#EEE' }} />
                    <View style={{ marginTop: 10, width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>UPCOMING EVENTS</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, color: '#BABABA' }}>10.05.2020</Text>
                            <Image style={{ height: 12, width: 12, resizeMode: 'contain', marginLeft: 5 }} source={require('../assets/down.png')} />
                        </View>
                    </View>
                    <View style={{ marginTop: 10, paddingHorizontal: wp('1%') }}>
                        <View style={{ width: wp('88%'), flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderColor: '#EEE', borderTopWidth: 2 }}>
                            <View style={{ width: wp('12%'), justifyContent: 'center', alignItems: 'flex-start' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 24, color: '#D0D0D0', lineHeight: 25 }}>10</Text>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 10, color: '#D0D0D0' }}>AM</Text>
                                </View>
                            </View>
                            <View style={{
                                width: wp('76%'), paddingHorizontal: wp('1.5%'), paddingVertical: 5, borderRadius: 4, borderColor: '#FFC803', borderRightWidth: 4, justifyContent: 'center', alignItems: 'flex-start', backgroundColor: '#FFF6D7',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 1,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 1.41,

                                elevation: 2,
                            }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>Interview - Senior Software Developer</Text>
                                <View style={{ width: wp('73%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, color: '#FFC803' }}>10:00 - 11:00 AM</Text>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#FFC803' }}>I{'&'}M Bank</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: wp('88%'), marginTop: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderColor: '#EEE', borderTopWidth: 2 }}>
                            <View style={{ width: wp('12%'), justifyContent: 'center', alignItems: 'flex-start' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 24, color: '#D0D0D0', lineHeight: 25 }}>12</Text>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 10, color: '#D0D0D0' }}>AM</Text>
                                </View>
                            </View>
                            <View style={{
                                width: wp('76%'), paddingHorizontal: wp('1.5%'), paddingVertical: 5, borderRadius: 4, borderColor: '#D0D0D0', borderRightWidth: 4, justifyContent: 'center', alignItems: 'flex-start', backgroundColor: '#F7F7F7',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 1,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 1.41,

                                elevation: 2,
                            }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>Software Testing</Text>
                                <View style={{ width: wp('73%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, color: '#D0D0D0' }}>12:00 - 13:00 PM</Text>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#D0D0D0' }}>E-LEARNING DEVELOP...</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: wp('88%'), marginTop: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderColor: '#EEE', borderTopWidth: 2 }}>
                            <View style={{ width: wp('12%'), justifyContent: 'center', alignItems: 'flex-start' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 24, color: '#D0D0D0', lineHeight: 25 }}>16</Text>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 10, color: '#D0D0D0' }}>AM</Text>
                                </View>
                            </View>
                            <View style={{
                                width: wp('76%'), paddingHorizontal: wp('1.5%'), paddingVertical: 5, borderRadius: 4, borderColor: '#FFC803', borderRightWidth: 4, justifyContent: 'center', alignItems: 'flex-start', backgroundColor: '#FFF6D7',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 1,
                                },
                                shadowOpacity: 0.20,
                                shadowRadius: 1.41,

                                elevation: 2,
                            }}>
                                <Text style={{ fontFamily: 'SF-Pro-Display-Medium', fontSize: 14, color: '#636363' }}>Interview - Backend team lead</Text>
                                <View style={{ width: wp('73%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 12, color: '#FFC803' }}>10:00 - 11:00 AM</Text>
                                    <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 12, color: '#FFC803' }}>VAB Bank</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{height:hp('8%')}}/>
            </ScrollView>
            <View style={{
                position: 'absolute', left: 0, bottom: 0, width: wp('100%'), height: 55, backgroundColor: '#fff', borderTopLeftRadius: 15, borderTopRightRadius: 15,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.27,
                shadowRadius: 4.65,
                elevation: 6,
                flexDirection: 'row', justifyContent: 'space-around', alignItems: 'flex-end', paddingBottom: 4
            }}>
                <TouchableOpacity onPress={()=>navigation.navigate('Jobs')} style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 12 }} source={require('../assets/doc.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>JOBS</Text>
                </TouchableOpacity>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 18 }} source={require('../assets/chat.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>MESSAGES</Text>
                </View>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 18 }} source={require('../assets/news.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#FFBC01', marginTop: 4 }}>MY FUZU</Text>
                </View>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 16, width: 18 }} source={require('../assets/lap.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>COURSES</Text>
                </View>
                <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image style={{ height: 18, width: 18 }} source={require('../assets/more.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Display-Regular', fontSize: 10, lineHeight: 11, color: '#636363', marginTop: 4 }}>MORE</Text>
                </View>
            </View>
        </View>
    );
};

export default home;
