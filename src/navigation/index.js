import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../screens/home'
import Jobs from '../screens/jobs';
import Detail from '../screens/detail';
import Login from '../screens/login';
import Register from '../screens/register';
import Forgot from '../screens/forgot';
import Proifile from '../screens/profile';
import Verify from '../screens/verify';
import Reset from '../screens/reset';
import Apply from '../screens/apply';


const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                <Stack.Screen name="Forgot" component={Forgot} options={{ headerShown: false }} />
                <Stack.Screen name="Verify" component={Verify} options={{ headerShown: false }} />
                <Stack.Screen name="Reset" component={Reset} options={{ headerShown: false }} />
                <Stack.Screen name="Profile" component={Proifile} options={{ headerShown: false }} />
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen name="Jobs" component={Jobs} options={{ headerShown: false }} />
                <Stack.Screen name="Detail" component={Detail} options={{ headerShown: false }} />
                <Stack.Screen name="Apply" component={Apply} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;