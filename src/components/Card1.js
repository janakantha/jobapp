import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const Card1 = () => {
    return (
        <View>
            <ImageBackground
                style={{ width: wp('85%'), height: hp('23%'), marginHorizontal: 20, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}
                resizeMode='stretch'
                source={require('../assets/upd1.png')}
            >
                <View style={{ height: hp('23%'), position: 'absolute', left: 0, top: 0, zIndex: 2, justifyContent: 'space-between', alignItems: 'flex-start', paddingVertical: 10, paddingLeft: 10 }}>
                    <View style={{ justifyContent: 'flex-start' }}>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Semibold', fontSize: 16, color: '#fff' }}>Go premium!</Text>
                        <Text style={{ fontFamily: 'SF-Pro-Text-Bold', fontSize: 12, color: '#636363' }}>Join the millions of Fuzu members!</Text>
                    </View>
                    <Text style={{ fontFamily: 'SF-Pro-Text-Regular', fontSize: 12, color: '#fff' }}>Start your free 1-month trial today.</Text>
                </View>
                <View style={{ zIndex: 1, height: hp('23%'), justifyContent: 'flex-end' }}>
                    <Image style={{ height: hp('19%'), width: wp('42%'), resizeMode: 'stretch' }} source={require('../assets/cup.png')} />
                </View>
            </ImageBackground>
        </View>
    );
};

export default Card1;
