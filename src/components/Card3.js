import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const Card3 = () => {
  return (
    <View style={{
        width: wp('85%'), height: hp('23%'), backgroundColor: '#F8F8F8', borderRadius: 16, marginRight: 20, borderColor: '#000', borderWidth: 2,
        shadowColor: 'rgba(0,0,0,0.25)', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,

    }}>
        <View style={{ height: hp('22%'), position: 'absolute', left: 0, top: 0, zIndex: 2, justifyContent: 'space-between', alignItems: 'flex-start', paddingVertical: 10, paddingLeft: 10 }}>
            <View style={{ justifyContent: 'flex-start' }}>
                <Text style={{ fontFamily: 'SF-Pro-Text-Semibold', fontSize: 16, color: '#636363' }}>Complete Python Developer in 2020</Text>
                <View style={{ width: 90, height: 18, backgroundColor: '#17C3B2', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'SF-Pro-Text-Semibold', fontSize: 10, color: '#F8F8F8' }}>NEW COURSE</Text>
                </View>
            </View>
            <View style={{ justifyContent: 'flex-start' }}>
                <Text style={{ fontFamily: 'SF-Pro-Display-Bold', fontSize: 16, color: '#636363' }}>$12.99</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image style={{ height: 15, width: 15, resizeMode: 'contain', marginRight: 5 }} source={require('../assets/clock.png')} />
                    <Text style={{ fontFamily: 'SF-Pro-Text-Regular', fontSize: 12, color: '#636363' }}>3 days left at this price!</Text>
                </View>
            </View>
        </View>
        <View style={{ zIndex: 1, height: hp('23%'), justifyContent: 'flex-end' }}>
            <Image style={{ height: hp('17%'), width: wp('58%'), resizeMode: 'stretch' }} source={require('../assets/pyt.png')} />
        </View>
    </View>
  );
};

export default Card3;
