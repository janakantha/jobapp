import React from 'react';
import { View, Text, StatusBar, Image, ImageBackground, TouchableOpacity, ScrollView } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const Card2 = () => {
    return (
        <View>
            <ImageBackground
                style={{ width: wp('85%'), height: hp('23%'), marginRight: 20 }}
                resizeMode='stretch'
                source={require('../assets/upd2.png')}
            >
                <View style={{ height: hp('23%'), justifyContent: 'space-between', alignItems: 'flex-start', paddingVertical: 10, paddingLeft: 10 }}>
                    <Text style={{ fontFamily: 'SF-Pro-Text-Semibold', fontSize: 16, color: '#fff' }}>United Against Corona</Text>
                    <Image style={{ height: 30, width: wp('76%'), resizeMode: 'contain', marginLeft: 10 }} source={require('../assets/labels.png')} />
                </View>
            </ImageBackground>
        </View>
    );
};

export default Card2;
