/**
 * @format
 */

import React from 'react';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';

import NvigatigationRouter from './src/navigation'

const app = () => (<NvigatigationRouter />);

AppRegistry.registerComponent(appName, () => app);
